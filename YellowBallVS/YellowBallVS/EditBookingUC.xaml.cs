﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YellowBallVS
{
    /// <summary>
    /// Interaction logic for EditBookingUC.xaml
    /// </summary>
    public partial class EditBookingUC : UserControl
    {
        public EditBookingUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            gridEdit.Visibility = Visibility.Hidden;
        }

        private void show_Click(object sender, RoutedEventArgs e)
        {
            EditDialog dlg = new EditDialog();
            dlg.Show();
        }
    }
}
