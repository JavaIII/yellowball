﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YellowBallLibrary;

namespace YellowBallVS
{ 
    public partial class MainWindow : Window
    { 
        public MainWindow()
        {
            InitializeComponent();
            //Globals.ctx = new FinalProjectWPFEntities();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            BookNowUC bookNow = new BookNowUC();
            gridTotal.Children.Add(bookNow);
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            EditBookingUC edit = new EditBookingUC();
            gridTotal.Children.Add(edit);
        }
    }
}
